FROM python:3.11

COPY pyproject.toml poetry.lock ./

RUN pip install poetry && poetry export --without-hashes --output /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir /project_cft

WORKDIR /project_cft

COPY . .

RUN chmod a+x ./docker/app.sh

CMD ["gunicorn", "src.main:app", "--workers", "1", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind=0.0.0.0:8000"]