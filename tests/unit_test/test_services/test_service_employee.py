"""Unit tests for EmployeesService"""

import pytest

from src.services import EmployeesService
from src.models import Employees
from src.exceptions import UserIsNotPresentException, ValidationException
from src.user.auth import verify_password


@pytest.mark.parametrize(
    "employee_id,login,exception",
    [
        (1, "test_user", None),
        (2, "test_user_2", None),
        (7, "....", UserIsNotPresentException)
    ],
)
async def test_get(
    employee_id: int,
    login: str,
    exception: Exception | None,
) -> None:
    """
        Test for obtaining employee authentication data

        :param employee_id: int
        :param login: str
        :param exception: Exception | None
        :return: None
    """
    if exception:
        with pytest.raises(exception):
            assert await EmployeesService.get(employee_id=employee_id)
    else:
        employee: Employees = await EmployeesService.get(employee_id=employee_id)
        assert employee.login == login
        assert employee.password


@pytest.mark.parametrize(
    "login,password,exception",
    [
        ("user321", "user321", None),
        ("user321", "user321", ValidationException),
    ],
)
async def test_add(
        login: str,
        password: str,
        exception: Exception | None
) -> None:
    """
        Test for adding employee authentication data

        :param login: str
        :param password: str
        :param exception: Exception | None
        :return: None
    """
    if exception:
        with pytest.raises(exception):
            assert await EmployeesService.add(login=login, password=password)
    else:
        employee_id: int = await EmployeesService.add(login=login, password=password)
        employee: Employees = await EmployeesService.get(employee_id=employee_id)
        assert employee.login == login
        assert employee.password


@pytest.mark.parametrize(
    "employee_id,password,is_present",
    [
        (3, "user321", True),
        (18, "user321", False),
    ],
)
async def test_update(
    employee_id: int,
    password: str,
    is_present: bool,
) -> None:
    """
        Employee password change test

        :param employee_id: int
        :param password: str
        :param is_present: bool
        :return: None
    """
    employee_id: int = await EmployeesService.update(employee_id=employee_id, password=password)
    if is_present:
        employee: Employees = await EmployeesService.get(employee_id=employee_id)
        assert verify_password(plain_password=password, hashed_password=employee.password)
    else:
        assert employee_id is None


@pytest.mark.parametrize(
    "employee_id,is_present",
    [
        (3, True),
        (18, False),
    ],
)
async def test_delete(
    employee_id: int,
    is_present: bool,
) -> None:
    """
        Employee deletion test

        :param employee_id: int
        :param is_present: bool
        :return: None
    """
    if is_present:
        await EmployeesService.delete(employee_id=employee_id)
    with pytest.raises(UserIsNotPresentException):
        await EmployeesService.get(employee_id=employee_id)


