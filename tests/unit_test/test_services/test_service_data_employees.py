"""Unit tests for DataEmployeesService"""

from datetime import date

import pytest

from sqlalchemy.exc import IntegrityError

from src.services import DataEmployeesService
from src.repositories.data_employees import DataEmployeesRepository
from src.models import DataEmployees
from src.exceptions import NotFoundException


@pytest.mark.parametrize(
    "employee_id,first_name,last_name,salary,date_promotion,exception",
    [
        (1, "Eva", "Morgan", 20000.00, "2024-09-10", None),
        (7, "...", "...", 20000.00, "2024-09-10", NotFoundException)
    ]
)
async def test_get(
    employee_id: int,
    first_name: str,
    last_name: str,
    salary: float,
    date_promotion: date,
    exception: Exception | None,
) -> None:
    """
        Test for getting data from the data employee table

        :param employee_id: int
        :param first_name: str
        :param last_name: str
        :param salary: float
        :param date_promotion: date
        :param exception: Exception | None
        :return: None
    """
    if exception:
        with pytest.raises(exception):
            assert await DataEmployeesService.get(employee_id=employee_id)
    else:
        data: DataEmployees = await DataEmployeesService.get(employee_id=employee_id)
        assert data.first_name == first_name
        assert data.last_name == last_name
        assert data.salary == salary
        assert date.strftime(data.date_promotion, '%Y-%m-%d') == date_promotion


@pytest.mark.parametrize(
    "employee_id,first_name,last_name,salary,date_promotion,exception",
    [
        (3, "Kylian", "Mbappe", 20000.00, date(year=2024, month=6, day=17), None),
        (5, "Erling", "Haaland", 50000.00, date(year=2024, month=10, day=12), IntegrityError),
    ]
)
async def test_add(
    employee_id: int,
    first_name: str,
    last_name: str,
    salary: float,
    date_promotion: date,
    exception: Exception | None
) -> None:
    """
        Test for adding data to the data employee table

        :param employee_id: int
        :param first_name: str
        :param last_name: str
        :param salary: float
        :param date_promotion: date
        :param exception: Exception | None
        :return: None
    """
    if exception:
        with pytest.raises(exception):
            await DataEmployeesService.add(
                employee_id=employee_id,
                first_name=first_name,
                last_name=last_name,
                salary=salary,
                date_promotion=date_promotion
            )
    else:
        data_employee_id: int = await DataEmployeesService.add(
            employee_id=employee_id,
            first_name=first_name,
            last_name=last_name,
            salary=salary,
            date_promotion=date_promotion
        )
        employee_data: DataEmployees = await DataEmployeesRepository.find_one_or_none(id=data_employee_id)
        assert employee_data.first_name == first_name
        assert employee_data.last_name == last_name
        assert employee_data.salary == salary
        assert employee_data.date_promotion == date_promotion


@pytest.mark.parametrize(
    "employee_id,first_name,last_name,salary,date_promotion,is_present",
    [
        (2, "Jude", "Bellingham", 90000.00, "2024-09-16", True),
        (90, None, None, 90000.00, "2024-09-16", False),
    ]
)
async def test_update(
    employee_id: int,
    first_name: str,
    last_name: str,
    salary: float,
    date_promotion: date,
    is_present: bool,
) -> None:
    """
        Test for changing data in the data employee table

        :param employee_id: int
        :param first_name: str
        :param last_name: str
        :param salary: float
        :param date_promotion: date
        :param is_present: bool
        :return: None
    """
    employee_data: DataEmployees = await DataEmployeesService.update(
        employee_id=employee_id,
        data={
            "first_name": first_name,
            "last_name": last_name,
            "salary": salary,
            "date_promotion": date_promotion,
        }
    )
    if is_present:
        assert employee_data.first_name == first_name
        assert employee_data.last_name == last_name
        assert employee_data.salary == salary
        assert date_promotion == date.strftime(employee_data.date_promotion, '%Y-%m-%d')
    else:
        assert employee_data is None


@pytest.mark.parametrize(
    "employee_id,is_present",
    [
        (2, True),
        (18, False),
    ],
)
async def test_delete(employee_id, is_present) -> None:
    """
        Test for deleting data in the data employee table

        :param employee_id: int
        :param is_present: bool
        :return: None
    """
    if is_present:
        await DataEmployeesService.delete(employee_id=employee_id)
    with pytest.raises(NotFoundException):
        await DataEmployeesService.get(employee_id=employee_id)
