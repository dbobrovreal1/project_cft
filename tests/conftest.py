"""Pytest configuration"""

import json
import pytest
import sqlalchemy
import uvloop

from datetime import datetime
from httpx import AsyncClient, ASGITransport
from sqlalchemy import insert
from typing import List

from src.database import Base, engine, async_session_maker
from src.config import settings
from src.models import Employees, DataEmployees
from src.main import app as fastapi_app


@pytest.fixture(scope="session", autouse=True)
async def database_preparation():
    """Database preparation. Adding test data"""
    assert settings.mode == 'test'

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

    def open_json(file: str):
        with open(f'tests/layout_{file}.json', 'r', encoding='utf-8') as file_json:
            return json.load(file_json)

    employee: List[dict] = open_json('employees')
    data_employee: List[dict] = open_json('data_employee')

    for data in data_employee:
        data['date_promotion'] = datetime.strptime(data['date_promotion'], "%Y-%m-%d")

    data_models: List[tuple] = [(Employees, employee), (DataEmployees, data_employee)]

    async with async_session_maker() as session:
        for model, values in data_models:
            query: sqlalchemy.Insert = insert(model).values(values)
            await session.execute(query)

        await session.commit()


@pytest.fixture(scope="session")
def event_loop_policy() -> uvloop.EventLoopPolicy:
    """Taken from the documentation for pytest-asyncio"""
    return uvloop.EventLoopPolicy()


@pytest.fixture(scope="function")
async def async_client()-> AsyncClient:
    """Creating a text client"""
    async with AsyncClient(transport=ASGITransport(app=fastapi_app), base_url="http://test") as ac:
        yield ac


@pytest.fixture(scope="session")
async def authenticated_ac() -> AsyncClient:
    """Creating an authorized text client"""
    async with AsyncClient(transport=ASGITransport(app=fastapi_app), base_url="http://test") as ac:
        await ac.post("/auth/login", json={
            "login": "test_user",
            "password": "user",
        })
        assert ac.cookies["access_token"]
        yield ac


@pytest.fixture(scope="session")
async def authenticated_ac_admin()-> AsyncClient:
    """Creating an authorized admin client"""
    async with AsyncClient(transport=ASGITransport(app=fastapi_app), base_url="http://test") as ac:
        await ac.post("/auth/login", json={
            "login": settings.admin.login,
            "password": settings.admin.password,
        })
        assert ac.cookies["access_token"]
        yield ac
