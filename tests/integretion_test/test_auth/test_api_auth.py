""" Integretion_test api auth"""

import pytest
from httpx import AsyncClient, Response


@pytest.mark.parametrize(
    "login,password,status_code",
    [
        ("test_user", "user", 200),
        ("test_user_2", "user2", 200),
        ("vasiliev_v.v", "vasya", 401),
    ]
)
async def test_login_endpoint(
    login: str,
    password: str,
    status_code: int,
    async_client: AsyncClient
) -> None:
    """
    Test login endpoint

    :param login: str
    :param password: str
    :param status_code: int
    :param async_client: AsyncClient
    :return : None
    """
    response: Response = await async_client.post("/auth/login", json={
        "login": login,
        "password": password,
    })

    assert response.status_code == status_code


@pytest.mark.parametrize(
    "login,password,status_code",
    [
        ("new_user", "new_user", 201),
        ("new_user", "new_user", 409),
    ]
)
async def test_register_endpoint(
    login: str,
    password: str,
    status_code: int,
    authenticated_ac_admin: AsyncClient
) -> None:
    """
    Test for registering a user with administrator rights

    :param login: str
    :param password: str
    :param status_code: int
    :param authenticated_ac_admin: AsyncClient
    :return: None
    """
    response: Response = await authenticated_ac_admin.post("/auth/register", json={
        "login": login,
        "password": password,
    })

    assert response.status_code == status_code


@pytest.mark.parametrize(
    "login,password,status_code",
    [("new_user_2", "new_user_2", 403),]
)
async def test_register_endpoint_no_rights(
    login: str,
    password: str,
    status_code: int,
    authenticated_ac: AsyncClient
) -> None:
    """
    Test for registering a user without administrator rights

    :param login: str
    :param password: str
    :param status_code: int
    :param authenticated_ac: AsyncClient
    :return: None
    """
    response: Response = await authenticated_ac.post("/auth/register", json={
        "login": login,
        "password": password,
    })

    assert response.status_code == status_code


@pytest.mark.parametrize(
    "login,password,status_code",
    [("new_user_2", "new_user_2", 401),]
)
async def test_register_endpoint_not_authorized(
    login: str,
    password: str,
    status_code: int,
    async_client: AsyncClient
) -> None:
    """
    User registration test if there is no authorization

    :param login: str
    :param password: str
    :param status_code: int
    :param async_client: AsyncClient
    :return: None
    """
    response: Response = await async_client.post("/auth/register", json={
        "login": login,
        "password": password,
    })

    assert response.status_code == status_code
