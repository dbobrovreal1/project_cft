""" Integretion_test api data employee with administrator rights"""

import pytest

from httpx import AsyncClient, Response

from datetime import date, datetime

from src.services import DataEmployeesService
from src.exceptions import NotFoundException
from src.models import DataEmployees


@pytest.mark.parametrize(
    "employee_id,first_name,last_name,salary,date_promotion,status_code",
    [
        (1, "Eva", "Morgan", 20000.00, "2024-09-10", 200),
    ])
async def test_salary_and_info_employees(
    employee_id: int,
    first_name: str,
    last_name: str,
    salary: float,
    date_promotion: date,
    status_code: int,
    authenticated_ac: AsyncClient
) -> None:
    """
        Salary and Employee Information Test

        :param employee_id: int,
        :param first_name: str,
        :param last_name: str,
        :param salary: float,
        :param date_promotion: date,
        :param status_code: int,
        :param authenticated_ac: AsyncClient
        :return None
    """
    response: Response = await authenticated_ac.get("/data")

    assert response.status_code == status_code

    data: dict = response.json()

    assert data["employee_id"] == employee_id
    assert data["first_name"] == first_name
    assert data["last_name"] == last_name
    assert data["salary"] == salary
    assert data["date_promotion"] == date_promotion


@pytest.mark.parametrize(
    "employee_id,first_name,last_name,salary,date_promotion,status_code",
    [
        (2, "Leo", "Messi", 50000.00, "2024-09-19", 409),
        (3, "Leo", "Messi", 50000.00, "2024-09-19", 201),
        (3, "Leo", "Messi", 50000.00, "2024-09-19", 409),
    ])
async def test_add_data_employee(
    employee_id: int,
    first_name: str,
    last_name: str,
    salary: float,
    date_promotion: date,
    status_code: int,
    authenticated_ac_admin: AsyncClient,
    async_client: AsyncClient
) -> None:
    """
        Test for adding employee information

        :param employee_id: int,
        :param first_name: str,
        :param last_name: str,
        :param salary: float,
        :param date_promotion: date,
        :param status_code: int,
        :param authenticated_ac_admin: AsyncClient
        :param async_client: AsyncClient
        :return None
    """
    response: Response = await authenticated_ac_admin.post(
        "/data",
        params={
            "employee_id": employee_id
        },
        json={
            "first_name": first_name,
            "last_name": last_name,
            "salary": salary,
            "date_promotion": date_promotion
        }
    )

    assert response.status_code == status_code

    data: DataEmployees = await DataEmployeesService.get(employee_id=employee_id)

    if response.status_code == 200:
        assert data.employee_id == employee_id
        assert data.first_name == first_name
        assert data.last_name == last_name
        assert data.salary == salary
        assert date_promotion == datetime.strftime(data.date_promotion, '%Y-%m-%d')


@pytest.mark.parametrize(
    "employee_id,first_name,last_name,salary,date_promotion,status_code",
    [
        (3, "Harry", "Kane", 50000.00, "2024-10-19", 200),
        (3, None, None, 90000.00, "2025-10-19", 200),
        (15, "Harry", "Kane", 50000.00, "2024-10-19", 401),
    ])
async def test_update_data_employee(
    employee_id: int,
    first_name: str,
    last_name: str,
    salary: float,
    date_promotion: date,
    status_code: int,
    authenticated_ac_admin: AsyncClient,
) -> None:
    """
        Test for changing employee information

        :param employee_id: int,
        :param first_name: str,
        :param last_name: str,
        :param salary: float,
        :param date_promotion: date,
        :param status_code: int,
        :param authenticated_ac_admin: AsyncClient
        :return None
    """
    response: Response = await authenticated_ac_admin.patch(
        '/data',
        params={
            "employee_id": employee_id
        },
        json={
            "first_name": first_name,
            "last_name": last_name,
            "salary": salary,
            "date_promotion": date_promotion
        }
    )

    assert response.status_code == status_code


@pytest.mark.parametrize(
    "employee_id,status_code",
    [
        (3, 204),
    ])
async def test_delete_data_employee(
    employee_id: int,
    status_code: int,
    authenticated_ac_admin: AsyncClient
) -> None:
    """
        Test for deleting employee information

        :param employee_id: int,
        :param status_code: int,
        :param authenticated_ac_admin: AsyncClient
        :return None
    """
    response: Response = await authenticated_ac_admin.delete(
        '/data',
        params={
            "employee_id": employee_id
        },
    )

    assert response.status_code == status_code

    with pytest.raises(NotFoundException):
        await DataEmployeesService.get(employee_id=employee_id)
