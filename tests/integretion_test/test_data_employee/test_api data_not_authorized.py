""" Integretion_test api data employee not authorized"""

import pytest

from httpx import AsyncClient, Response

from datetime import date


@pytest.mark.parametrize(
    "employee_id,first_name,last_name,salary,date_promotion,status_code",
    [(3, "Leo", "Messi", 50000.00, "2024-09-19", 401),]
)
async def test_data_employee_not_authorized(
    employee_id: int,
    first_name: str,
    last_name: str,
    salary: float,
    date_promotion: date,
    status_code: int,
    async_client: AsyncClient
) -> None:
    """
    A test to check all the endpoints
    associated with employee data, as the same
    result is expected.

    :param employee_id: int
    :param first_name: str
    :param last_name: str
    :param salary: float
    :param date_promotion: date
    :param status_code: int
    :param async_client:
    :return: None
    """
    response_get: Response = await async_client.get("/data")
    assert response_get.status_code == status_code

    response_post: Response = await async_client.post(
        "/data",
        params={
            "employee_id": employee_id
        },
        json={
            "first_name": first_name,
            "last_name": last_name,
            "salary": salary,
            "date_promotion": date_promotion
        }
    )

    assert response_post.status_code == status_code

    response_patch: Response = await async_client.patch(
        "/data",
        params={
            "employee_id": employee_id
        },
        json={
            "first_name": first_name,
            "last_name": last_name,
            "salary": salary,
            "date_promotion": date_promotion
        }
    )

    assert response_patch.status_code == status_code

    response_delete: Response = await async_client.delete("/data", params={'employee_id': employee_id})
    assert response_delete.status_code == status_code


