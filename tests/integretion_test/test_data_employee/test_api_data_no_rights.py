""" Integretion_test api data employee without administrator rights"""

import pytest

from httpx import AsyncClient, Response

from datetime import date


@pytest.mark.parametrize(
    "employee_id,first_name,last_name,salary,date_promotion,status_code",
    [(3, "Leo", "Messi", 50000.00, "2024-09-19", 403),]
)
async def test_add_data_employee_no_rights(
    employee_id: int,
    first_name: str,
    last_name: str,
    salary: float,
    date_promotion: date,
    status_code: int,
    authenticated_ac: AsyncClient
) -> None:
    """
        Test for adding employee information

        :param employee_id: int,
        :param first_name: str,
        :param last_name: str,
        :param salary: float,
        :param date_promotion: date,
        :param status_code: int,
        :param authenticated_ac: AsyncClient
        :return None
    """
    response: Response = await authenticated_ac.post(
        "/data",
        params={
            "employee_id": employee_id
        },
        json={
            "first_name": first_name,
            "last_name": last_name,
            "salary": salary,
            "date_promotion": date_promotion
        }
    )

    assert response.status_code == status_code


@pytest.mark.parametrize(
    "employee_id,first_name,last_name,salary,date_promotion,status_code",
    [
        (3, "Harry", "Kane", 50000.00, "2024-10-19", 403),
    ]
)
async def test_update_data_employee_no_rights(
    employee_id: int,
    first_name: str,
    last_name: str,
    salary: float,
    date_promotion: date,
    status_code: int,
    authenticated_ac: AsyncClient
) -> None:
    """
        Test for changing employee information

        :param employee_id: int,
        :param first_name: str,
        :param last_name: str,
        :param salary: float,
        :param date_promotion: date,
        :param status_code: int,
        :param authenticated_ac_admin: AsyncClient
        :return None
    """
    response: Response = await authenticated_ac.patch(
        '/data',
        params={
            "employee_id": employee_id
        },
        json={
            "first_name": first_name,
            "last_name": last_name,
            "salary": salary,
            "date_promotion": date_promotion
        }
    )

    assert response.status_code == status_code


@pytest.mark.parametrize(
    "employee_id,status_code",
    [(3, 403),]
)
async def test_delete_data_employee_no_rights(
    employee_id: int,
    status_code: int,
    authenticated_ac: AsyncClient
) -> None:
    """
        Test for deleting employee information

        :param employee_id: int,
        :param status_code: int,
        :param authenticated_ac: AsyncClient
        :return None
    """
    response: Response = await authenticated_ac.delete(
        '/data',
        params={
            "employee_id": employee_id
        },
    )

    assert response.status_code == status_code
