""" Integretion_test api employee without administrator rights"""

import pytest

from httpx import AsyncClient, Response


@pytest.mark.parametrize(
    "employee_id,password,status_code",
    [
        (2, 'user', 403),
    ],
)
async def test_update_employee_endpoint_no_rights(
    employee_id: int,
    password: str,
    status_code: int,
    authenticated_ac: AsyncClient
) -> None:
    """
        Test for changing employee information

        :param employee_id: int
        :param password: str
        :param status_code: int
        :param authenticated_ac: AsyncClient
        :return: None
    """
    response: Response = await authenticated_ac.patch(
        "/employee",
        params={
            "employee_id": employee_id
        },
        json={
            "password": password,
        }
    )

    assert response.status_code == status_code


@pytest.mark.parametrize(
    "employee_id,status_code",
    [
        (3, 403),
    ],
)
async def test_delete_employee_endpoint_no_rights(
    employee_id: int,
    status_code: int,
    authenticated_ac: AsyncClient
) -> None:
    """
        Test for deleting employee
        :param employee_id: int
        :param status_code: int
        :param authenticated_ac: AsyncClient
        :return: None
    """
    response: Response = await authenticated_ac.delete(
        "/employee",
        params={
            "employee_id": employee_id
        }
    )

    assert response.status_code == status_code
