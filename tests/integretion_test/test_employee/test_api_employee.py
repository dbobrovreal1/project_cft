""" Integretion_test api employee with administrator rights"""

import pytest

from httpx import AsyncClient, Response

from src.services import DataEmployeesService, EmployeesService
from src.exceptions import NotFoundException, UserIsNotPresentException


@pytest.mark.parametrize(
    "employee_id,password,status_code",
    [
        (2, 'user', 200),
        (5, 'user123', 401),
    ]
)
async def test_update_employee_endpoint(
    employee_id: int,
    password: str,
    status_code: int,
    authenticated_ac_admin: AsyncClient
) -> None:
    """
        Test for changing employee information
        :param employee_id: int
        :param password: str
        :param status_code: int
        :param authenticated_ac_admin: AsyncClient
        :return: None
    """
    response: Response = await authenticated_ac_admin.patch(
        "/employee",
        params={
            "employee_id": employee_id
        },
        json={
            "password": password,
        }
    )

    assert response.status_code == status_code


@pytest.mark.parametrize(
    "employee_id,status_code",
    [
        (4, 204),
        (5, 401),
    ]
)
async def test_delete_employee_endpoint(
    employee_id: int,
    status_code: int,
    authenticated_ac_admin: AsyncClient
) -> None:
    """
        Test for deleting employee
        :param employee_id: int
        :param status_code: int
        :param authenticated_ac_admin: AsyncClient
        :return: None
    """
    response: Response = await authenticated_ac_admin.delete(
        "/employee",
        params={
            "employee_id": employee_id
        }
    )

    assert response.status_code == status_code

    with pytest.raises(NotFoundException):
        await DataEmployeesService.get(employee_id=employee_id)

    with pytest.raises(UserIsNotPresentException):
        await EmployeesService.get(employee_id=employee_id)
