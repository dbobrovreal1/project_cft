""" Integretion_test api employee not authorized"""

import pytest

from httpx import AsyncClient, Response


@pytest.mark.parametrize("employee_id,password,status_code", [
    (2, 'user', 401),
])
async def test_update_employee_endpoint_not_authorized(
    employee_id: int,
    password: str,
    status_code: int,
    async_client: AsyncClient
) -> None:
    """
        Test for changing employee information
        :param employee_id: int
        :param password: str
        :param status_code: int
        :param async_client: AsyncClient
        :return: None
    """
    response: Response = await async_client.patch(
        "/employee",
        params={
            "employee_id": employee_id
        },
        json={
            "password": password,
        }
    )

    assert response.status_code == status_code


@pytest.mark.parametrize(
    "employee_id,status_code",
    [
        (3, 401),
    ],
)
async def test_delete_employee_endpoint_not_authorized(
    employee_id: int,
    status_code: int,
    async_client: AsyncClient
) -> None:
    """
        Test for deleting employee
        :param employee_id: int
        :param status_code: int
        :param async_client: AsyncClient
        :return: None
    """
    response: Response = await async_client.delete(
        "/employee",
        params={
            "employee_id": employee_id
        }
    )

    assert response.status_code == status_code
