from src.dao.base import BaseDao
from src.models.data_employees import DataEmployees


class DataEmployeesRepository(BaseDao):
    model = DataEmployees
