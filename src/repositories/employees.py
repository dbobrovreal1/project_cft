from src.dao.base import BaseDao
from src.models.employees import Employees


class EmployeesRepository(BaseDao):
    model = Employees
