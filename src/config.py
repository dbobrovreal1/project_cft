from typing import Literal
from pydantic_settings import BaseSettings, SettingsConfigDict


class DBSettings(BaseSettings):
    """
    Configuration for the database
    """
    model_config = SettingsConfigDict(env_prefix='DB_', env_file=".env", extra='allow')

    host: str
    port: int
    name: str
    user: str
    password: str
    echo: bool = False

    @property
    def database_url(self):
        return f"postgresql+asyncpg://{self.user}:{self.password}@{self.host}:{self.port}/{self.name}"


class TestDBSettings(BaseSettings):
    """Configuration for the test database"""
    model_config = SettingsConfigDict(env_prefix='TEST_DB_', env_file=".env", extra='allow')

    host: str
    port: int
    name: str
    user: str
    password: str
    echo: bool = False

    @property
    def database_url(self):
        return f"postgresql+asyncpg://{self.user}:{self.password}@{self.host}:{self.port}/{self.name}"


class JWTSettings(BaseSettings):
    """Configuration JWT settings"""
    model_config = SettingsConfigDict(env_prefix='JWT_', env_file=".env", extra='allow')
    algorithm: str
    secret_key: str


class AdminSettings(BaseSettings):
    """Configuration Admin settings"""
    model_config = SettingsConfigDict(env_prefix='ADMIN_', env_file=".env", extra='allow')
    login: str
    password: str


class Settings(BaseSettings):
    """Settings class"""
    mode: Literal['dev', 'test']
    db: DBSettings = DBSettings()
    test_db: TestDBSettings = TestDBSettings()
    jwt: JWTSettings = JWTSettings()
    admin: AdminSettings = AdminSettings()

    model_config = SettingsConfigDict(env_file=".env")


settings = Settings()
