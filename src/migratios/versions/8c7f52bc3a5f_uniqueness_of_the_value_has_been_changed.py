"""uniqueness of the value has been changed

Revision ID: 8c7f52bc3a5f
Revises: 3a7f4e563b1a
Create Date: 2024-05-04 14:39:02.915557

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '8c7f52bc3a5f'
down_revision: Union[str, None] = '3a7f4e563b1a'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint(None, 'employees', ['login'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'employees', type_='unique')
    # ### end Alembic commands ###
