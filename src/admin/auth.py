from sqladmin.authentication import AuthenticationBackend

from starlette.requests import Request
from starlette.responses import RedirectResponse

from src.exceptions import UserIsNotPresentException
from src.user.auth import authenticate_admin, create_access_token


class AdminAuth(AuthenticationBackend):
    async def login(self, request: Request) -> bool:
        form = await request.form()
        login, password = form["username"], form["password"]

        user = authenticate_admin(login, password)

        if not user:
            raise UserIsNotPresentException

        access_token = create_access_token({"sub": 'admin'})

        request.session.update({"token": access_token})

        return True

    async def logout(self, request: Request) -> bool:
        request.session.clear()
        return True

    async def authenticate(self, request: Request) -> bool:
        token = request.session.get("token")

        if not token:
            return RedirectResponse(request.url_for("admin:login"), status_code=302)

        return True


authentication_backend = AdminAuth(secret_key="...")
