from sqladmin import ModelView

from src.models import Employees, DataEmployees


class EmployeesAdmin(ModelView, model=Employees):
    column_list = [Employees.id, Employees.login, Employees.data_employee]
    column_details_exclude_list = [Employees.password]
    name = "Employee"
    name_plural = "Employees"
    icon = "fa-solid fa-user"


class DataEmployeesAdmin(ModelView, model=DataEmployees):
    column_list = [c.name for c in DataEmployees.__table__.c] + [DataEmployees.employee]
    can_delete = True
    name = "Salary data"
    name_plural = "Employee salary data"
    icon = "fa fa-address-card"

