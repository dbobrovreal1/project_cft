from src.repositories.employees import EmployeesRepository
from src.user.auth import get_password_hash, authenticate_employee, authenticate_admin
from src.exceptions import ValidationException, UserIsNotPresentException


class EmployeesService:
    @classmethod
    async def get(cls, employee_id: int):
        employee = await EmployeesRepository.find_one_or_none(id=employee_id)
        if not employee:
            raise UserIsNotPresentException
        return employee

    @classmethod
    async def add(
            cls,
            login: str,
            password: str
    ) -> None:
        existing_employees = await EmployeesRepository.find_one_or_none(login=login)

        if existing_employees:
            raise ValidationException

        hashed_password = get_password_hash(password)
        return await EmployeesRepository.add(
            login=login,
            password=hashed_password,
        )

    @classmethod
    async def login(
            cls,
            login: str,
            password: str
    ) -> str:
        employee = await authenticate_employee(login=login, password=password)

        if not employee:
            employee = authenticate_admin(login=login, password=password)
            if not employee:
                raise UserIsNotPresentException
            param_token = {"sub": 'admin'}
        else:
            param_token = {'sub': str(employee.id)}

        return param_token

    @classmethod
    async def update(
            cls,
            employee_id,
            password: str
    ) -> int:
        hashed_password = get_password_hash(password)
        return await EmployeesRepository.update_by_id(
            model_id=employee_id,
            password=hashed_password,
        )

    @classmethod
    async def delete(cls, employee_id):
        return await EmployeesRepository.delete(employee_id=employee_id)
