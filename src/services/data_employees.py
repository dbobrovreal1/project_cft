from src.repositories.data_employees import DataEmployeesRepository
from src.exceptions import NotFoundException

from datetime import datetime


class DataEmployeesService:
    @classmethod
    async def get(cls, employee_id):
        data = await DataEmployeesRepository.find_one_or_none(employee_id=employee_id)
        if not data:
            raise NotFoundException
        return data

    @classmethod
    async def add(cls, **data):
        return await DataEmployeesRepository.add(**data)

    @classmethod
    async def update(cls, employee_id, data):
        if data.get('date_promotion'):
            data['date_promotion'] = datetime.strptime(data['date_promotion'], '%Y-%m-%d')

        return await DataEmployeesRepository.update(employee_id=employee_id, data=data)

    @classmethod
    async def delete(cls, **data):
        return await DataEmployeesRepository.delete(**data)

