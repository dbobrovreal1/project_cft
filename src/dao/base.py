from typing import Any

from sqlalchemy import select, insert, update, delete

from src.database import async_session_maker


class BaseDao:
    """BaseDao is designed to work with database tables"""
    model = None

    @classmethod
    async def find_all(cls, **data) -> dict:
        """
        Find all rows in the database

        :param data: dictionary with data
        :return: dict
        """
        async with async_session_maker() as session:
            query = select(cls.model.__table__.columns).filter_by(**data)
            result = await session.execute(query)
            return result.mappings().all()

    @classmethod
    async def find_one_or_none(cls, **filter_by) -> Any:
        """
        Find one or none rows in the database

        :param filter_by: dictionary with data
        :return: One record or none
        """
        async with async_session_maker() as session:
            query = select(cls.model).filter_by(**filter_by)
            result = await session.execute(query)
            return result.scalar_one_or_none()

    @classmethod
    async def add(cls, **data) -> int:
        """
        Add a new row to the database

        :param data: dictionary with data
        :return: id of the new row
        """
        async with async_session_maker() as session:
            query = insert(cls.model).values(**data).returning(cls.model.id)
            result = await session.execute(query)
            await session.commit()
            return result.scalar()

    @classmethod
    async def update(cls, employee_id: int, data: dict) -> Any:
        """
        Update the row in the database

        :param employee_id: id of the employee
        :param data: dictionary with data
        :return: cheating record model
        """
        async with async_session_maker() as session:
            query = update(cls.model).where(cls.model.employee_id == employee_id).values(**data).returning(cls.model)
            result = await session.execute(query)
            await session.commit()
            return result.scalar()

    @classmethod
    async def update_by_id(cls, model_id: int, **data: dict) -> int:
        """
        Update the row in the database by id

        :param model_id: id of the model
        :param data: dictionary with data
        :return: cheating record model ин id
        """
        async with async_session_maker() as session:
            query = update(cls.model).where(cls.model.id == model_id).values(**data).returning(cls.model.id)
            result = await session.execute(query)
            await session.commit()
            return result.scalar()

    @classmethod
    async def delete(cls, employee_id: int) -> None:
        """
        Delete the row in the database by id

        :param employee_id: id of the employee
        :return: None
        """
        async with async_session_maker() as session:
            query = delete(cls.model).where(cls.model.id == employee_id)
            await session.execute(query)
            await session.commit()
