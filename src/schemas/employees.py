from pydantic import BaseModel


class SEmployeesAuth(BaseModel):
    login: str
    password: str


class SEmployeesUpdate(BaseModel):
    password: str
