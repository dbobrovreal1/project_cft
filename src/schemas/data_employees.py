from datetime import date
from pydantic import BaseModel


class SDataEmployeesOut(BaseModel):
    id: int
    employee_id: int
    first_name: str
    last_name: str
    salary: float
    date_promotion: date


class SDataEmployeesAdd(BaseModel):
    first_name: str
    last_name: str
    salary: float
    date_promotion: date


class SDataEmployeesUpdate(BaseModel):
    first_name: str | None = None
    last_name: str | None = None
    salary: float | None = None
    date_promotion: date | None = None
