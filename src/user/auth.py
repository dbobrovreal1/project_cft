from datetime import datetime, timedelta

from jose import jwt
from passlib.context import CryptContext

from src.config import settings
from src.repositories.employees import EmployeesRepository

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def create_access_token(data: dict) -> str:
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=30)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode,
        settings.jwt.secret_key,
        settings.jwt.algorithm,
    )

    return encoded_jwt


def authenticate_admin(
    login: str,
    password: str
):
    if not (login == settings.admin.login and password == settings.admin.password):
        return False

    return True


async def authenticate_employee(
    login: str,
    password: str
):
    user = await EmployeesRepository.find_one_or_none(login=login)
    if not (user and verify_password(password, user.password)):
        return None

    return user





