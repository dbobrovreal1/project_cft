from sqlalchemy.orm import mapped_column, Mapped, relationship
from sqlalchemy import Date, ForeignKey

from datetime import date

from src.database import Base

from typing import TYPE_CHECKING

# import locale
# locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
#
# if TYPE_CHECKING:
#     from src.models import Employees


class DataEmployees(Base):
    """
    Model DataEmploees for table data_employees
    """
    __tablename__ = "data_employees"
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    employee_id: Mapped[int] = mapped_column(
        ForeignKey('employees.id', ondelete="CASCADE"),
        unique=True,
    )
    first_name: Mapped[str] = mapped_column(nullable=False)
    last_name: Mapped[str] = mapped_column(nullable=False)
    salary: Mapped[float] = mapped_column(nullable=False)
    date_promotion: Mapped[date] = mapped_column(Date)

    employee: Mapped["Employees"] = relationship(
        "Employees",
        back_populates="data_employee",
    )

    def __str__(self) -> str:
        # salary = locale.currency(self.salary, grouping=True)
        return f"{self.first_name} {self.last_name} {self.salary} salary. Date promotion {self.date_promotion}"

