from sqlalchemy.orm import mapped_column, Mapped, relationship

from src.database import Base

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from src.models import DataEmployees


class Employees(Base):
    """
        Model Employees for table employees
    """
    __tablename__ = "employees"
    id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    login: Mapped[str] = mapped_column(nullable=False, unique=True)
    password: Mapped[str] = mapped_column(nullable=False)

    data_employee: Mapped["DataEmployees"] = relationship(
        "DataEmployees",
        back_populates="employee",
        uselist=False,
        cascade="all, delete"
    )

    def __str__(self):
        return f"A user with a username {self.login}"
