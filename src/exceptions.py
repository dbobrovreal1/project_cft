from fastapi import HTTPException, status


class ApiError(HTTPException):
    status_code = 500
    detail = ""

    def __init__(self):
        super().__init__(status_code=self.status_code, detail=self.detail)


class TokenAbsentException(ApiError):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "Token is missing"


class TokenExpiredException(ApiError):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "Token has expired"


class IncorrectTokenFormatException(ApiError):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "Invalid token format"


class UserIsNotPresentException(ApiError):
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = "User is not"


class ValidationException(ApiError):
    status_code = status.HTTP_409_CONFLICT
    detail = 'A user with this username exists'


class NotFoundException(ApiError):
    status_code = status.HTTP_404_NOT_FOUND
    detail = 'Not found'


class NotEnoughRights(ApiError):
    status_code = status.HTTP_403_FORBIDDEN
    detail = 'Not enough rights'


class DataFillingConflict(ApiError):
    status_code = status.HTTP_409_CONFLICT
    detail = 'Data filling conflict'
