"""
Dependencies of endpoints
"""

from fastapi import Request, Depends

from jose import jwt, JWTError, ExpiredSignatureError

from src.config import settings
from src.models import Employees, DataEmployees
from src.repositories.data_employees import DataEmployeesRepository
from src.repositories.employees import EmployeesRepository
from src.exceptions import (
    TokenAbsentException,
    IncorrectTokenFormatException,
    TokenExpiredException,
    UserIsNotPresentException,
    NotEnoughRights,
    DataFillingConflict
)


def get_token(request: Request) -> str:
    """
    Method checks whether
    the user's token exists in cookies

    :param request: Request Parameters
    :return: token (str) - user token
    """
    token: str | None = request.cookies.get('access_token')
    if not token:
        raise TokenAbsentException
    return token


async def get_current_token(token: str = Depends(get_token)) -> Employees:
    """
    Method checks the validity
    of the token and what is
    the current user right now

    :param token: (str) - user token
    :return: user (Employees) - model employees
    """
    try:
        payload: dict = jwt.decode(
            token,
            settings.jwt.secret_key,
            settings.jwt.algorithm,
        )
    except ExpiredSignatureError:
        raise TokenExpiredException
    except JWTError:
        raise IncorrectTokenFormatException

    user_id: str = payload.get("sub")

    if not user_id or user_id == 'admin':
        raise UserIsNotPresentException

    user: Employees = await EmployeesRepository.find_one_or_none(id=int(user_id))

    if not user:
        raise UserIsNotPresentException

    return user


async def get_current_token_admin(token: str = Depends(get_token)) -> None:
    """
    Method checks for the validity
    of the token and whether
    the current user is an administrator.
    If not, it will cause an error NotEnoughRights

    :param token: (str) - user token
    :return: None
    """
    try:
        payload: dict = jwt.decode(
            token,
            settings.jwt.secret_key,
            settings.jwt.algorithm,
        )
    except ExpiredSignatureError:
        raise TokenExpiredException
    except JWTError:
        raise IncorrectTokenFormatException

    sub_user: str = payload.get("sub")

    if sub_user != 'admin':
        raise NotEnoughRights


async def get_employee_by_id(request: Request) -> None:
    """
    Method checks whether such
    an employee exists in the database.
    If not, it will cause an error UserIsNotPresentException

    :param request: Request Parameters
    :return: None
    """
    employee_id: int = request.query_params.get("employee_id")

    employee: Employees = await EmployeesRepository.find_one_or_none(id=int(employee_id))
    if not employee:
        raise UserIsNotPresentException


async def get_employee_data(request: Request) -> None:
    """
    Method checks whether there
    is data about the employee.
    If there is, it will cause an error DataFillingConflict.

    :param request: Request Parameters
    :return: None
    """
    employee_id: int = request.query_params.get("employee_id")

    data_employee: DataEmployees = await DataEmployeesRepository.find_one_or_none(employee_id=int(employee_id))

    if data_employee:
        raise DataFillingConflict
