from src.api.employees import router as employees_router
from src.api.data_employees import router as data_employees_router
from src.api.auth import router as auth_router
