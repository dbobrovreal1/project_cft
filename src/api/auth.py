""" API authorization"""

from fastapi import APIRouter, Response, Depends
from starlette import status

from src.schemas.employees import SEmployeesAuth
from src.services.employees import EmployeesService
from src.user.auth import create_access_token
from src.api.dependencies import get_current_token_admin

router = APIRouter(
    prefix="/auth",
    tags=["User authorization"],
)


@router.post(
    "/register",
    status_code=status.HTTP_201_CREATED,
    dependencies=[Depends(get_current_token_admin)]
)
async def register_endpoint(
    employee_data: SEmployeesAuth,
) -> int:
    """
    Register a new employee\n
    !!!WARNING!!! \n
    Available only to the administrator
    """
    return await EmployeesService.add(
        login=employee_data.login,
        password=employee_data.password
    )


@router.post("/login", status_code=status.HTTP_200_OK)
async def login_endpoint(
    response: Response,
    authorization_parameters: SEmployeesAuth,
) -> str:
    """
    User authorization
    """
    param_token: str = await EmployeesService.login(
        login=authorization_parameters.login,
        password=authorization_parameters.password
    )
    user_token: str = create_access_token(param_token)
    response.set_cookie('access_token', user_token, httponly=True)

    return user_token


@router.post("/logout", status_code=status.HTTP_200_OK)
async def logout_endpoint(response: Response) -> None:
    """
    User logout
    """
    response.delete_cookie('access_token')
