"""
API for working with employee data
"""


from fastapi import APIRouter, Depends, status
from fastapi.encoders import jsonable_encoder

from src.models.employees import Employees
from src.schemas.data_employees import (
    SDataEmployeesOut,
    SDataEmployeesAdd,
    SDataEmployeesUpdate,
)
from src.api.dependencies import (
    get_current_token,
    get_current_token_admin,
    get_employee_by_id,
    get_employee_data,
)
from src.services.data_employees import DataEmployeesService


router: APIRouter = APIRouter(
    prefix="/data",
    tags=["Employee data"],
)


@router.get('', status_code=status.HTTP_200_OK, response_model=SDataEmployeesOut)
async def salary_and_info_employees(
    employee: Employees = Depends(get_current_token)
):
    """
    The endpoint for receiving
    employee salaries and the date
    of the next promotion
    """
    return await DataEmployeesService.get(employee_id=employee.id)


@router.post(
    '',
    status_code=status.HTTP_201_CREATED,
    dependencies=[
        Depends(get_current_token_admin),
        Depends(get_employee_by_id),
        Depends(get_employee_data),
    ],
)
async def add_data_employee(
    employee_id: int,
    data: SDataEmployeesAdd,
) -> int:
    """
    Endpoint for adding new employee data\n
    !!!WARNING!!! \n
    Available only to the administrator
    """
    return await DataEmployeesService.add(
        employee_id=employee_id,
        first_name=data.first_name,
        last_name=data.last_name,
        salary=data.salary,
        date_promotion=data.date_promotion,
    )


@router.patch(
    '',
    status_code=status.HTTP_200_OK,
    dependencies=[
        Depends(get_current_token_admin),
        Depends(get_employee_by_id),
    ],
    response_model=SDataEmployeesOut,
)
async def update_data_employee(
    employee_id: int,
    data: SDataEmployeesUpdate,
):
    """
    Endpoint for changing the current employee data\n
    !!!WARNING!!! \n
    Available only to the administrator
    """
    data: dict = jsonable_encoder(data, exclude_none=True)
    return await DataEmployeesService.update(
        employee_id=employee_id,
        data=data,
    )


@router.delete(
    '',
    status_code=status.HTTP_204_NO_CONTENT,
    dependencies=[
        Depends(get_current_token_admin),
        Depends(get_employee_by_id)
    ],
)
async def delete_data_employee(employee_id: int) -> None:
    """
    Endpoint for deleting employee data\n
    !!!WARNING!!! \n
    Available only to the administrator
    """
    return await DataEmployeesService.delete(employee_id=employee_id)
