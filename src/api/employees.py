"""
API for working with employee
"""

from fastapi import APIRouter, Depends, status

from src.schemas.employees import SEmployeesUpdate
from src.services.employees import EmployeesService
from src.api.dependencies import get_current_token_admin, get_employee_by_id

router = APIRouter(
    prefix="/employee",
    tags=["Employees"],
)


@router.patch(
    "",
    status_code=status.HTTP_200_OK,
    dependencies=[
            Depends(get_current_token_admin),
            Depends(get_employee_by_id),
        ]
)
async def update_employee_endpoint(
    employee_id: int,
    new_password: SEmployeesUpdate,
) -> int:
    """
     Endpoint for changing the user's password\n
     !!!WARNING!!! \n
    Available only to the administrator
    """
    return await EmployeesService.update(
        employee_id=employee_id,
        password=new_password.password
    )


@router.delete(
    "",
    status_code=status.HTTP_204_NO_CONTENT,
    dependencies=[
            Depends(get_current_token_admin),
            Depends(get_employee_by_id)
        ],
)
async def delete_employee_endpoint(employee_id: int) -> None:
    """
    Endpoint for deleting an employee from the database
    !!!WARNING!!! \n
    Available only to the administrator
    """
    return await EmployeesService.delete(employee_id=employee_id)
