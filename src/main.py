from sqladmin import Admin
from fastapi import FastAPI

from src.api import data_employees_router, employees_router, auth_router
from src.database import engine
from src.admin import EmployeesAdmin, DataEmployeesAdmin, authentication_backend


app = FastAPI()

app.include_router(auth_router)
app.include_router(employees_router)
app.include_router(data_employees_router)

admin = Admin(app, engine, authentication_backend=authentication_backend)

admin.add_view(EmployeesAdmin)
admin.add_view(DataEmployeesAdmin)
