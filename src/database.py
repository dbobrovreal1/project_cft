from sqlalchemy import NullPool
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine
from sqlalchemy.orm import DeclarativeBase

from src.config import settings


if settings.mode == 'test':
    database_url: str = settings.test_db.database_url
    database_parameters: dict = {
        "poolclass": NullPool,
        'echo': settings.db.echo,
    }
else:
    database_url: str = settings.db.database_url
    database_parameters: dict = {'echo': settings.db.echo}

engine = create_async_engine(database_url, **database_parameters)
async_session_maker = async_sessionmaker(engine, expire_on_commit=False)


class Base(DeclarativeBase):
    pass

